jQuery(document).ajaxComplete(function () {
  jQuery("#user-register-form").submit(function() {
    var errors = jQuery(".form-text.error, .form-select.error, .form-checkbox.error").size();
    if(errors == 0){
      var mail = jQuery("#edit-mail").val();
      dataLayer.push({
        'event': 'Registro_Voluntariado',
        'eventCategory': 'Registro_Voluntariado',
        'eventAction': 'Registro_exitoso',
        'eventLabel': mail
      });
    }
  });
});
