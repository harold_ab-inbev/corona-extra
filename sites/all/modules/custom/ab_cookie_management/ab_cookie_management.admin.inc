<?php

/**
 * @file
 * Admin functionality for age_checker module.
 */

/**
 * Form callback for age_checker admin settings.
 */
function ab_cookie_management_admin_form()
{
    global $theme;

    $form = array();

    $form['cookie_management_enabled'] = array(
        '#type'          => 'select',
        '#options'       => array(1 => 'On', 0 => 'Off'),
        '#title'         => t('Enabled'),
        '#description'   => t('Enable or Disable Cookie Banner.'),
        '#default_value' => variable_get('cookie_management_enabled', 0),
    );
    $form['cookie_management_site_code'] = array(
        '#title'         => t('Enter the site unique code'),
        '#type'          => 'textfield',
        '#maxlength'     => 255,
        '#default_value' => variable_get('cookie_management_site_code', 'xxxxxxxx'),
        '#required'      => true,
        '#description'   => t('Enter the site code'),
    );
    $form['cookie_management_css'] = array(
        '#title'         => t('Aditional CSS'),
        '#type'          => 'textarea',
        '#maxlength'     => 1000000,
        '#default_value' => variable_get('cookie_management_css', ''),
        '#required'      => true,
        '#description'   => t('Paste the CSS code from OneTrust'),
    );
    $form['cookie_management_js'] = array(
        '#title'         => t('Aditional js'),
        '#type'          => 'textarea',
        '#maxlength'     => 1000000,
        '#default_value' => variable_get('cookie_management_js', ''),
        '#required'      => true,
        '#description'   => t('Paste the JS code from OneTrust'),
    );

    return system_settings_form($form);
}

/**
 * Validate if user entered numeric values.
 */
function ab_cookie_management_admin_form_validate($form, &$form_state)
{

}
