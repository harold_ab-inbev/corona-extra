jQuery(document).ready(function() {
    jQuery(".paragraphs-items .field-item:first-child .field-name-field-imagen-region").waypoint(function() {
        jQuery(".paragraphs-items .field-item:first-child .field-name-field-imagen-region").addClass("fadeIn animated")
    }, {
        offset: "100%"
    }), jQuery(".paragraphs-items .field-item:nth-child(2) .field-name-field-imagen-region").waypoint(function() {
        jQuery(".paragraphs-items .field-item:nth-child(2) .field-name-field-imagen-region").addClass("fadeInUp animated")
    }, {
        offset: "100%"
    }), jQuery(".paragraphs-items .field-item:nth-child(3) .field-name-field-imagen-region").waypoint(function() {
        jQuery(".paragraphs-items .field-item:nth-child(3) .field-name-field-imagen-region").addClass("fadeIn animated")
    }, {
        offset: "100%"
    }), jQuery(".paragraphs-items .field-item:nth-child(4) .field-name-field-imagen-region").waypoint(function() {
        jQuery(".paragraphs-items .field-item:nth-child(4) .field-name-field-imagen-region").addClass("fadeIn animated")
    }, {
        offset: "100%"
    }), jQuery(".footer .icons .item").waypoint(function() {
        jQuery(".footer .icons .item").addClass("fadeIn animated")
    }, {
        offset: "100%"
    })

    jQuery(".footer a, a.logo").click(function(e){
      var href = jQuery(this).attr("href");
      e.preventDefault();
      dataLayer.push({
        'event': 'Exit_links',
        'eventCategory': 'Exit_links',
        'eventAction': 'Click',
        'eventLabel': href
      });
      window.open(href, "_blank");
    });
});
