<?php

/**
 * @file
 * Default theme implementation to display a single Drupal page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.tpl.php template in this directory.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/bartik.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['content']: The main content of the current page.
 * - $page['header']: Items for the header region.
 * - $page['footer']: Items for the footer region.
 *
 * @see bootstrap_preprocess_page()
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see bootstrap_process_page()
 * @see template_process()
 * @see html.tpl.php
 *
 * @ingroup templates
 */
?>
<header>
  <div class="<?php print $container_class; ?>">
    <div class="navbar-header">
      <?php if ($logo): ?>
        <a class="logo navbar-btn" href="https://www.mexicoextra.com/" title="<?php print t('Home'); ?>">
          <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
        </a>
      <?php endif; ?>
      <h1 style="display:none;"><?php print $site_name; ?></h1>
    </div>
    <?php print render($page['header']); ?>
  </div>
</header>




<div class="main-container <?php print $container_class; ?>">
  <div class="row">
    <section<?php print $content_column_class; ?>>
      <?php if (!empty($page['highlighted'])): ?>
        <div class="highlighted jumbotron"><?php print render($page['highlighted']); ?></div>
      <?php endif; ?>
      <?php if (!empty($breadcrumb)): print $breadcrumb;
      endif;?>
      <a id="main-content"></a>
      <?php print render($title_prefix); ?>
      <?php print render($title_suffix); ?>
      <?php print $messages; ?>
      <?php if (!empty($tabs)): ?>
        <?php print render($tabs); ?>
      <?php endif; ?>
      <?php if (!empty($page['help'])): ?>
        <?php print render($page['help']); ?>
      <?php endif; ?>
      <?php if (!empty($action_links)): ?>
        <ul class="action-links"><?php print render($action_links); ?></ul>
      <?php endif; ?>
      <iframe width="100%" height="720px" src="https://www.youtube.com/embed/PEBdy1cDiW8?controls=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
      <?php print render($page['content']); ?>
      <div class="addimg fadeIn animated"><img src="https://www.mexicoextra.com/sites/all/themes/custom/corona/img/MorganMaassen.jpg" /></div>
    </section>
  </div>
</div>
    <!-- Content Float -->
    <div class="flotante-registro">
      <span class="close"></span>
      <p class="copy">&Uacute;nete al movimiento para proteger el para&iacute;so.</p>
      <div id="registro-flotante" class="btn-registro">
        <a href="https://www.mexicoextra.com/user/register">INSCR&Iacute;BETE AQU&Iacute;</a>
      </div>
    </div>
<?php if (!empty($page['footer'])): ?>
  <footer class="footer <?php print $container_class; ?>">
    <?php print render($page['footer']); ?>
  </footer>
<?php endif; ?>
<script type="text/javascript">
        jQuery(document).ready(function(){
        
          function SHA256(s){
                var chrsz = 8;
                var hexcase = 0;

                function safe_add (x, y) {
                var lsw = (x & 0xFFFF) + (y & 0xFFFF);
                var msw = (x >> 16) + (y >> 16) + (lsw >> 16);
                return (msw << 16) | (lsw & 0xFFFF);
                }

                function S (X, n) { return ( X >>> n ) | (X << (32 - n)); }
                function R (X, n) { return ( X >>> n ); }
                function Ch(x, y, z) { return ((x & y) ^ ((~x) & z)); }
                function Maj(x, y, z) { return ((x & y) ^ (x & z) ^ (y & z)); }
                function Sigma0256(x) { return (S(x, 2) ^ S(x, 13) ^ S(x, 22)); }
                function Sigma1256(x) { return (S(x, 6) ^ S(x, 11) ^ S(x, 25)); }
                function Gamma0256(x) { return (S(x, 7) ^ S(x, 18) ^ R(x, 3)); }
                function Gamma1256(x) { return (S(x, 17) ^ S(x, 19) ^ R(x, 10)); }

                function core_sha256 (m, l) {
                var K = new Array(0x428A2F98, 0x71374491, 0xB5C0FBCF, 0xE9B5DBA5, 0x3956C25B, 0x59F111F1, 0x923F82A4, 0xAB1C5ED5, 0xD807AA98, 0x12835B01, 0x243185BE, 0x550C7DC3, 0x72BE5D74, 0x80DEB1FE, 0x9BDC06A7, 0xC19BF174, 0xE49B69C1, 0xEFBE4786, 0xFC19DC6, 0x240CA1CC, 0x2DE92C6F, 0x4A7484AA, 0x5CB0A9DC, 0x76F988DA, 0x983E5152, 0xA831C66D, 0xB00327C8, 0xBF597FC7, 0xC6E00BF3, 0xD5A79147, 0x6CA6351, 0x14292967, 0x27B70A85, 0x2E1B2138, 0x4D2C6DFC, 0x53380D13, 0x650A7354, 0x766A0ABB, 0x81C2C92E, 0x92722C85, 0xA2BFE8A1, 0xA81A664B, 0xC24B8B70, 0xC76C51A3, 0xD192E819, 0xD6990624, 0xF40E3585, 0x106AA070, 0x19A4C116, 0x1E376C08, 0x2748774C, 0x34B0BCB5, 0x391C0CB3, 0x4ED8AA4A, 0x5B9CCA4F, 0x682E6FF3, 0x748F82EE, 0x78A5636F, 0x84C87814, 0x8CC70208, 0x90BEFFFA, 0xA4506CEB, 0xBEF9A3F7, 0xC67178F2);
                var HASH = new Array(0x6A09E667, 0xBB67AE85, 0x3C6EF372, 0xA54FF53A, 0x510E527F, 0x9B05688C, 0x1F83D9AB, 0x5BE0CD19);
                var W = new Array(64);
                var a, b, c, d, e, f, g, h, i, j;
                var T1, T2;

                m[l >> 5] |= 0x80 << (24 - l % 32);
                m[((l + 64 >> 9) << 4) + 15] = l;

                for ( var i = 0; i<m.length; i+=16 ) {
                a = HASH[0];
                b = HASH[1];
                c = HASH[2];
                d = HASH[3];
                e = HASH[4];
                f = HASH[5];
                g = HASH[6];
                h = HASH[7];

                for ( var j = 0; j<64; j++) {
                if (j < 16) W[j] = m[j + i];
                else W[j] = safe_add(safe_add(safe_add(Gamma1256(W[j - 2]), W[j - 7]), Gamma0256(W[j - 15])), W[j - 16]);

                T1 = safe_add(safe_add(safe_add(safe_add(h, Sigma1256(e)), Ch(e, f, g)), K[j]), W[j]);
                T2 = safe_add(Sigma0256(a), Maj(a, b, c));

                h = g;
                g = f;
                f = e;
                e = safe_add(d, T1);
                d = c;
                c = b;
                b = a;
                a = safe_add(T1, T2);
                }

                HASH[0] = safe_add(a, HASH[0]);
                HASH[1] = safe_add(b, HASH[1]);
                HASH[2] = safe_add(c, HASH[2]);
                HASH[3] = safe_add(d, HASH[3]);
                HASH[4] = safe_add(e, HASH[4]);
                HASH[5] = safe_add(f, HASH[5]);
                HASH[6] = safe_add(g, HASH[6]);
                HASH[7] = safe_add(h, HASH[7]);
                }
                return HASH;
                }

                function str2binb (str) {
                var bin = Array();
                var mask = (1 << chrsz) - 1;
                for(var i = 0; i < str.length * chrsz; i += chrsz) {
                bin[i>>5] |= (str.charCodeAt(i / chrsz) & mask) << (24 - i % 32);
                }
                return bin;
                }

                function Utf8Encode(string) {
                string = string.replace(/\r\n/g,'\n');
                var utftext = '';

                for (var n = 0; n < string.length; n++) {

                var c = string.charCodeAt(n);

                if (c < 128) {
                utftext += String.fromCharCode(c);
                }
                else if((c > 127) && (c < 2048)) {
                utftext += String.fromCharCode((c >> 6) | 192);
                utftext += String.fromCharCode((c & 63) | 128);
                }
                else {
                utftext += String.fromCharCode((c >> 12) | 224);
                utftext += String.fromCharCode(((c >> 6) & 63) | 128);
                utftext += String.fromCharCode((c & 63) | 128);
                }

                }

                return utftext;
                }

                function binb2hex (binarray) {
                var hex_tab = hexcase ? '0123456789ABCDEF' : '0123456789abcdef';
                var str = '';
                for(var i = 0; i < binarray.length * 4; i++) {
                str += hex_tab.charAt((binarray[i>>2] >> ((3 - i % 4)*8+4)) & 0xF) +
                hex_tab.charAt((binarray[i>>2] >> ((3 - i % 4)*8 )) & 0xF);
                }
                return str;
                }

                s = Utf8Encode(s);
                return binb2hex(core_sha256(str2binb(s), s.length * chrsz));
                }
          console.log('Esta cargando...');
          
          jQuery('#registro-flotante a').click(function(){
            console.log('Click Label');
            setTimeout(function(){
              console.log('Ejecuto SetTime');
              jQuery( "#user-register-form" ).submit(function( event ) {
                console.log('Envio Form');
                event.preventDefault();
            
                var Rnombres = jQuery('.field-name-field-nombre input').val();
                var RapellidoP = jQuery('.field-name-field-apellido-paterno input').val();
                var RapellidoM = jQuery('.field-name-field-apellido-materno input').val();
                var Rgenero = jQuery('.field-name-field-g-nero select').val();
                var RCumple = jQuery('.field-name-field-fecha-de-nacimiento input').val();
     
                var Restado = jQuery('.field-name-field-estado select').val();
                var Rtelefono = jQuery('.field-name-field-tel-fonos input').val();
                var Remail = jQuery('.form-item-mail input').val();
            
                console.log( "nombres: "+Rnombres );
                console.log( "apellidoP: "+RapellidoP );
                console.log( "apellidoM: "+RapellidoM );
                console.log( "genero: "+Rgenero );
                console.log( "Cumple: "+RCumple );
                console.log( "estado: "+Restado );
                console.log( "telefono: "+Rtelefono );
                console.log( "email: "+Remail );  
                
                dataLayer.push({
                    'event': 'trackEvent',
                    'eventCategory': 'MexicoExtra', // Categoría del evento (String). Requerido.
                    'eventAction': 'registro',  // Acción o subcategoría del evento (String). Requerido.
                    'eventLabel': SHA256(Remail),  // Etiqueta de descripción del evento (String). Requerido.
                    'eventValue': '1'  // Valor o peso (importancia) del evento (String).
                    });
                jQuery.ajax({
                        url: 'https://empresarios.modelorama.com.mx/api/emailMarketing',
                        dataType: 'json',
                        type: 'post',
                        contentType: 'application/json',
                        data: JSON.stringify( {
                            email: Remail,
                            firstname: Rnombres,
                            lastname: Rapellido,
                            brand: "Corona",
                            campaign:"Mexico Extra",
                            mobile: Rtelefono,
                            location: Restado,
                            nationality:"Mexicana",
                            gender: Rgenero,
                            source:"Landing Page"
                        } ),
                        success: function( data, textStatus, jQxhr ){
                            console.log(data);
                        },
                        error: function( jqXhr, textStatus, errorThrown ){
                            console.log( errorThrown );
                        }
                    });
                    
              });
            }, 4000);
          });

        });
</script>


